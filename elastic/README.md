# ECS template

## Installation

Loading of the template into the ElasticSearch:

```
curl -XPOST 'localhost:9200/_template/lmio-ecs-template' --header "Content-Type: application/json" -d @'es-ecs-template.json'
```

The index pattern is `lmio-*` (basically any event from LogMan.io)

## Origin

The template has been built based on https://github.com/elastic/ecs `generated/elasticsearch/7/template.json`

